package com.curso.springboot.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.curso.springboot.api.entities.Curso;
import com.curso.springboot.api.repositories.CursoRepository;

@Component
@Profile(value = "dev")
public class TestaDataBase implements CommandLineRunner{
	
	@Autowired
	private CursoRepository cursoRepository;
	
	@Override
	public void run(String... args) throws Exception {
		
		Curso curso1 = new Curso("Graduação em ADS", "Humanas");
		Curso curso2 = new Curso("Graduação em Administração", "Exatas");
		Curso curso3 = new Curso("Graduação em Radiologia", "Humanas");
		Curso curso4 = new Curso("Graduação em Engenharia de Software", "Exatas");
		Curso curso5 = new Curso("Graduação em Ciência da Computação", "Humanas");
				
		cursoRepository.save(curso1);
		cursoRepository.save(curso2);
		cursoRepository.save(curso3);
		cursoRepository.save(curso4);
		cursoRepository.save(curso5);
						
	}

}
