package com.curso.springboot.api.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="grade")
public class GradeCurricular implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private String objetivo;
	
	@OneToOne
	@JoinColumn(
			name = "aluno_id",
			referencedColumnName = "id"
			)
	private Aluno aluno;
	
	@ManyToMany(mappedBy = "gradeCurricular")
	private Set<Materias> materias = new HashSet<>();
	
	public GradeCurricular() {
		// TODO Auto-generated constructor stub
	}
	
	public GradeCurricular(String objetivo, Aluno aluno) {
		
		this.objetivo = objetivo;
		this.aluno = aluno;
	}


	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Materias> getMaterias() {
		return materias;
	}

	public void setMaterias(Set<Materias> materias) {
		this.materias = materias;
	}
	

}
