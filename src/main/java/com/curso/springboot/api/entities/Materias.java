package com.curso.springboot.api.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="materia")
public class Materias implements Serializable{
	
private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private String objetivo;
	
	@ManyToMany
	@JoinTable(
			name = "grade_materia",
			joinColumns = {
					@JoinColumn(name = "materia_id", referencedColumnName = "id")
			},
			inverseJoinColumns = {
					@JoinColumn(name = "grade_id", referencedColumnName = "id")
			}
	)
	private Set<GradeCurricular> gradeCurricular = new HashSet<>();
	
	public Materias() {
		// TODO Auto-generated constructor stub
	}

	public Materias(String objetivo, Set<GradeCurricular> gradeCurricular) {
		
		this.objetivo = objetivo;
		this.gradeCurricular = gradeCurricular;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public Set<GradeCurricular> getGradeCurricular() {
		return gradeCurricular;
	}

	public void setGradeCurricular(Set<GradeCurricular> gradeCurricular) {
		this.gradeCurricular = gradeCurricular;
	}
	
	
}
