package com.curso.springboot.api.entities.mapper;

import org.springframework.stereotype.Service;

import com.curso.springboot.api.entities.Curso;
import com.curso.springboot.api.entities.dto.CursoDTO;

@Service
public class CursoMapper {
	
	public Curso mapCursoDTOToCurso(CursoDTO dto) {
		
		Curso curso = new Curso(dto.getNome(), dto.getArea());
		return curso;
	}

}
