package com.curso.springboot.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.springboot.api.entities.Aluno;

public interface AlunoRepository extends JpaRepository<Aluno, Integer>{

}
