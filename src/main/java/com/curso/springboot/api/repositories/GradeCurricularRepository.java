package com.curso.springboot.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.springboot.api.entities.GradeCurricular;

public interface GradeCurricularRepository extends JpaRepository<GradeCurricular, Integer>{

	
}
