package com.curso.springboot.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.springboot.api.entities.Materias;

public interface MateriasRepository extends JpaRepository<Materias, Integer>{

}
