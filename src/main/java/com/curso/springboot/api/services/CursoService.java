package com.curso.springboot.api.services;

import java.util.List;

import com.curso.springboot.api.entities.Curso;

public interface CursoService {
	
	public List<Curso> getCursos();
	public Curso save(Curso curso);

}
